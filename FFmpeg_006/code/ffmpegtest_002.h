#ifndef FFMPEGTEST_002_H
#define FFMPEGTEST_002_H

#include <QObject>
#include <thread>
#include <QImage>
#include <QWidget>
#include <QPainter>

extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libavutil/pixfmt.h"
#include "libswscale/swscale.h"
#include "libavdevice/avdevice.h"
#include "libavutil/pixdesc.h"
#include "libavutil/hwcontext.h"
#include "libavutil/opt.h"
#include "libavutil/avassert.h"
#include "libavutil/imgutils.h"
}

class FFmpegtest_002:public QWidget
{
    Q_OBJECT
public:
    FFmpegtest_002(QWidget *parent = 0);
    ~FFmpegtest_002(){}
    void init();
    void play();
protected:
    void paintEvent(QPaintEvent *event);
private:
    void decodec();
signals:
    void signalDraw();
public slots:
    void slotDraw();
private:
    std::thread m_decodecThread;
    AVFormatContext *pAVFormatCtx=NULL;
    AVCodecContext *pAVCodecCtx;
    SwsContext *pSwsCtx;

    uint8_t *pRgbBuffer;
    AVPacket packet;    
    AVFrame *pAVFrame;
    AVFrame *pAVFrameRGB;
    int iVideoIndex = -1;
    int iAudioIndex = -1;
    QImage m_image;
    bool isFinish;

};

#endif // FFMPEGTEST_002_H
