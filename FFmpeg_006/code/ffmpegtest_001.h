#ifndef FFMPEGTEST_001_H
#define FFMPEGTEST_001_H

#include<QObject>
#include<QWidget>
#include<QLabel>
#include<QGridLayout>



class FFmpegTest_001:public QWidget
{
public:
    FFmpegTest_001(QWidget *parent=0);
    ~FFmpegTest_001(){}

    void AnalysisVedio();
    void delay(int msec);

private:
    QLabel *label;
    QGridLayout *layout;
};

#endif // FFMPEGTEST_001_H
