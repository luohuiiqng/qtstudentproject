#include "ffmpegtest_001.h"
#include <QPixmap>
#include <QTime>
#include <QCoreApplication>
#include <QDebug>

extern "C"
{
#include<libavcodec/avcodec.h>
#include<libavformat/avformat.h>
#include<libswscale/swscale.h>
#include<libavutil/imgutils.h>
}


FFmpegTest_001::FFmpegTest_001(QWidget *parent):QWidget(parent)
{
    layout =new QGridLayout(this);
    label = new QLabel(this);
    layout->addWidget(label);
    this->setGeometry(0,0,200,200);
}

void FFmpegTest_001::delay(int msec)
{
    QTime dieTime = QTime::currentTime().addMSecs(msec);
    while( QTime::currentTime() < dieTime )
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

void FFmpegTest_001::AnalysisVedio()
{
    AVFormatContext *pFormatCtx;//存储音视频封装格式中包含的信息
    int videoIndex = -1;//视频帧索引
    AVCodecContext *pCodecCtx;//视频流编码结构
    AVCodec *pCodec;//视频解码器
    AVFrame *pFrame,*pFrameRGB;//帧缓存?
    unsigned char *out_buffer;//保存图像数据
    AVPacket *packet;//保存复用(demuxer）之后,解码之前的数据
    int ret,got_picture;

    struct SwsContext *img_convert_ctx;//主要用于视频图像的转换

    char filepath[] = "D://MP4//hello.mp4";//当前目录为构建目录

    //注册FFMpeg的库
    //av_register_all();

    //打开音视频流并获取音视频流信息
    pFormatCtx = avformat_alloc_context();
    //打开音视频流
    if(avformat_open_input(&pFormatCtx,filepath,nullptr,nullptr)!=0)
    {
        qDebug()<<"Couldn't open input stream.\n";
        return;
    }

    //查找视频流的起始位置以及查找视频解码器
    for(int i=0;i<(int)pFormatCtx->nb_streams;i++)
    {
        //查找到视频流时退出循环
        if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            videoIndex = i;
            break;
        }
    }

    //查找视频解码器
    pCodecCtx = pFormatCtx->streams[videoIndex]->codec;//获取视频流编码结构
    qDebug()<<"ididdidi:"<<pCodecCtx->codec_id<<"\n";
    pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
    if(pCodec = nullptr)
    {
        qDebug()<<"Codec not found.\n";
        return ;
    }

    //打开解码器
    int msg = avcodec_open2(pCodecCtx,pCodec,nullptr);
    if(msg<0)
    {
        qDebug()<<"Could not open codec::"<<msg<<"\n";
        return;
    }

    //打印视频信息
    qDebug()<<"----------- File Information ------------------\n";
    av_dump_format(pFormatCtx,0,filepath,0);
    qDebug()<<"----------- File Information ------------------\n";

    //视频编码的同时处理图片像素数据
    pFrame = av_frame_alloc();
    pFrameRGB = av_frame_alloc();

    //创建动态内存，创建存储图像数据的空间
    out_buffer = (unsigned char *)av_malloc((size_t)av_image_get_buffer_size(AV_PIX_FMT_RGB32,
                                                                             pCodecCtx->width,
                                                                             pCodecCtx->height,1));
    //存储一帧像素数据缓冲区
    av_image_fill_arrays(pFrameRGB->data,pFrameRGB->linesize,out_buffer,
                         AV_PIX_FMT_RGB32,pCodecCtx->width,pCodecCtx->height,1);
    packet = (AVPacket *)av_malloc(sizeof(AVPacket));

    //初始化img_convert_ctx结构
    img_convert_ctx =sws_getContext(pCodecCtx->width,pCodecCtx->height,pCodecCtx->pix_fmt,
                                    pCodecCtx->width,pCodecCtx->height,AV_PIX_FMT_RGB32,
                                    SWS_BICUBIC,nullptr,nullptr,nullptr);

    //av_read_frame读取一帧未解码的数据
    while(av_read_frame(pFormatCtx,packet)>=0)
    {

        //如果是视频数据
        if(packet->stream_index == videoIndex)
        {
            //解码一帧视频数据
            ret =avcodec_decode_video2(pCodecCtx,pFrame,&got_picture,packet);

            if(ret<0)
            {
                qDebug()<<"Decode Error.\n";
                return ;
            }

            if(got_picture)
            {
                sws_scale(img_convert_ctx,(const unsigned char*const*)pFrame->data,pFrame->linesize,0
                          ,pCodecCtx->height,pFrameRGB->data,pFrameRGB->linesize);

                QImage img((uchar*)pFrameRGB->data[0],pCodecCtx->width,pCodecCtx->height,QImage::Format_RGB32);
                label->setPixmap(QPixmap::fromImage(img));
                delay(40);
            }
        }
        av_free_packet(packet);
    }

    //释放申请的内存空间
    sws_freeContext(img_convert_ctx);
    av_frame_free(&pFrameRGB);
    av_frame_free(&pFrame);
    avcodec_close(pCodecCtx);
    avformat_close_input(&pFormatCtx);

}
