#include "ffmpegtest_002.h"
#include <QDebug>

FFmpegtest_002::FFmpegtest_002(QWidget *parent ):QWidget(parent)
{
    connect(this,&FFmpegtest_002::signalDraw,this,&FFmpegtest_002::slotDraw);
}

void FFmpegtest_002::init()
{
    std::string file = "E:/workSpace/qtstudentproject/MP4/hello.mp4";
    //描述多媒体文件的构成及其基本信息

    if(avformat_open_input(&pAVFormatCtx,file.data(),NULL,NULL) != 0)
    {
        qDebug()<<"open file faile!\n";
        avformat_free_context(pAVFormatCtx);
        return;
    }

    //读取一部分音视频数据并且获得一些相关的信息
    if(avformat_find_stream_info(pAVFormatCtx,NULL)<0)
    {
        qDebug()<<"vformat find stream faile!\n";
        avformat_close_input(&pAVFormatCtx);
        return;
    }

    //根据解码器枚举类型找到解码器
    AVCodec *pAVCodec,*pAUCodec;
    int ret = av_find_best_stream(pAVFormatCtx,AVMEDIA_TYPE_VIDEO,-1,-1,&pAVCodec,0);//视频
    if(ret<0)
    {
        qDebug()<<"av_find_best_stream failture vedio!\n";
        avformat_close_input(&pAVFormatCtx);
        return;
    }
    iVideoIndex = ret;

    pAVCodec = avcodec_find_decoder(pAVFormatCtx->streams[iVideoIndex]->codecpar->codec_id);
    if(pAVCodec == NULL)
    {
        qDebug()<<"not find decoder!\n";
        return ;
    }


    qDebug()<<"avcodec_open2 pAVCodec->name:"<<QString::fromStdString(pAVCodec->name);

    ret = av_find_best_stream(pAVFormatCtx,AVMEDIA_TYPE_AUDIO,-1,-1,&pAUCodec,0);//音频
    if(ret<0)
    {
        qDebug()<<"av_find_best_steam audio failture!\n";
    }

    iAudioIndex = ret;
  //  pAUCodec = avcodec_find_decoder(pAVFormatCtx->streams[iAudioIndex]->codecpar->codec_id);
    //if(pAUCodec ==NULL)
    //{
    //    qDebug()<<"not find audio decoder!\n";
     //   return;
   // }


    if(pAVFormatCtx->streams[iVideoIndex]->avg_frame_rate.den !=0)
    {
        float fps_ = pAVFormatCtx->streams[iVideoIndex]->avg_frame_rate.num/pAVFormatCtx->streams[iVideoIndex]->avg_frame_rate.den;
        qDebug()<<"fps:"<<fps_;
    }


    int64_t video_length_sec_ = pAVFormatCtx->duration/AV_TIME_BASE;
    qDebug()<<"video_Length_sec_:"<<video_length_sec_;
    pAVCodecCtx = avcodec_alloc_context3(pAVCodec);
    if(pAVCodec == NULL)
    {
        qDebug()<<"get pAVCodecCtx faile!\n";
        avformat_close_input(&pAVFormatCtx);
        return;
    }
    ret = avcodec_parameters_to_context(pAVCodecCtx,pAVFormatCtx->streams[iVideoIndex]->codecpar);
    if(ret <0 )
    {
        qDebug()<<"avcodec_parameters_to_context faile!\n";
        avformat_close_input(&pAVFormatCtx);
        return;
    }

    if(avcodec_open2(pAVCodecCtx,pAVCodec,NULL)<0)
    {
        qDebug()<<"avcodec_open2 faile!\n";
        return;
    }

    //为解码帧分片内存

    //AVFrame存放从AVPacket中解码出来的原始数据
    pAVFrame = av_frame_alloc();
    pAVFrameRGB = av_frame_alloc();

    //用于视频图像的转换，将源数据转换为RGB32的目标数据
    pSwsCtx = sws_getContext(pAVCodecCtx->width,pAVCodecCtx->height,pAVCodecCtx->pix_fmt,
                             pAVCodecCtx->width,pAVCodecCtx->height,AV_PIX_FMT_RGB32,
                             SWS_BICUBIC,NULL,NULL,NULL);
   int m_size = av_image_get_buffer_size(AVPixelFormat(AV_PIX_FMT_RGB32),pAVCodecCtx->width,pAVCodecCtx->height,1);
   pRgbBuffer = (uint8_t *)(av_malloc(m_size));

   //为已经分配的空间结构体AVPicture挂上一段用于保存数据的空间
   avpicture_fill((AVPicture*)pAVFrameRGB,pRgbBuffer,AV_PIX_FMT_BGR32,pAVCodecCtx->width,pAVCodecCtx->height);

   //AVPacket用来解码数据
   av_new_packet(&packet,pAVCodecCtx->width*pAVCodecCtx->height);
}

void FFmpegtest_002::play()
{
    m_decodecThread = std::thread([this]()
    {
        decodec();
    });
    m_decodecThread.detach();
}

void FFmpegtest_002::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setBrush(Qt::black);
    painter.drawRect(0,0,this->width(),this->height());

    if(m_image.size().width()<=0)
    {
        return;
    }

    //比例缩放
    QImage img = m_image.scaled(this->size(),Qt::KeepAspectRatio);
    int x=this->width()-img.width();
    int y=this->height()-img.height();
    x/=2;
    y/=2;

    painter.drawImage(QPoint(x,y),img);
}

void FFmpegtest_002::decodec()
{
    //读取码流中视频帧
    while(true)
    {
        int ret = av_read_frame(pAVFormatCtx,&packet);
        if(ret !=0)
        {
            qDebug()<<"file end!\n";
            isFinish = !isFinish;
            return;
        }
        if(packet.stream_index != iVideoIndex)
        {
            av_packet_unref(&packet);
            continue;
        }

        int iGotPic = AVERROR(EAGAIN);

        //解码一帧视频数据
        iGotPic = avcodec_send_packet(pAVCodecCtx,&packet);
        if(iGotPic!=0)
        {
            qDebug()<<"avcodec_send_packet error!\n";
            continue;
        }

        iGotPic = avcodec_receive_frame(pAVCodecCtx,pAVFrame);
        if(iGotPic == 0)
        {
            //转换像素
            sws_scale(pSwsCtx,(uint8_t const * const *)pAVFrame->data,pAVFrame->linesize,0,
                      pAVCodecCtx->height,pAVFrameRGB->data,pAVFrameRGB->linesize);

            //构造QImage
            QImage img(pRgbBuffer,pAVCodecCtx->width,pAVCodecCtx->height,QImage::Format_ARGB32);
            qDebug()<<"decode img!\n";
            m_image = img;
            emit signalDraw();
        }
        else
        {
            qDebug()<<"decode error";
        }

        av_packet_unref(&packet);
        std::this_thread::sleep_for(std::chrono::milliseconds(25));
    }

    //资源回收
    av_free(pAVFrame);
    av_free(pAVFrameRGB);
    sws_freeContext(pSwsCtx);
    avcodec_close(pAVCodecCtx);
    avformat_close_input(&pAVFormatCtx);
}

void FFmpegtest_002::slotDraw()
{
    this->update();
}
