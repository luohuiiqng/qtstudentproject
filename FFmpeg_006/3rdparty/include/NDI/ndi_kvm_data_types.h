﻿#pragma once
#pragma pack(push, 1)
enum e_message_type_t : uint8_t
{	// The pointer type is a monochrome mouse pointer, which is a monochrome bitmap. The bitmap's 
	// size is specified by width and height in a 1 bits per pixel (bpp) device independent bitmap 
	// (DIB) format AND mask that is followed by another 1 bpp DIB format XOR mask of the same size.
	e_message_type_cursor_shape_monochrome = 0,

	// The pointer type is a color mouse pointer, which is a color bitmap. The bitmap's size is 
	// specified by width and height in a 32 bpp ARGB DIB format.
	e_message_type_cursor_shape_color = 1,

	// The pointer type is a masked color mouse pointer. A masked color mouse pointer is a 32 bpp ARGB 
	// format bitmap with the mask value in the alpha bits. The only allowed mask values are 0 and 0xFF. 
	// When the mask value is 0, the RGB value should replace the screen pixel. When the mask value is 
	// 0xFF, an XOR operation is performed on the RGB value and the screen pixel; the result replaces 
	// the screen pixel.
	e_message_type_cursor_shape_masked_color = 2,

	// This is send the mouse position
	e_message_type_cursor_position = 3,

	// Click message
	e_message_type_click_down_left = 4,
	e_message_type_click_down_middle = 5,
	e_message_type_click_down_right = 6,
	e_message_type_click_up_left = 7,
	e_message_type_click_up_middle = 8,
	e_message_type_click_up_right = 9,

	// Mouse wheel
	e_message_type_wheel_vertical = 10,
	e_message_type_wheel_horizontal = 11,

	// Keyboard
	e_message_type_keyboard = 12,

	// Clipboard
	e_message_type_clipboard = 13,

	// Touch
	e_message_type_touch = 14,
	
	// Custom
	e_message_type_drive = 80,
	e_message_type_peninfo = 81,
    e_message_type_logout = 82,
};

struct message_type_t
{	// The message type
	e_message_type_t message_type;
};

struct message_type_cursor_t : public message_type_t
{	// The width and height
	uint16_t size[2];

	// The hot-stop position
	uint16_t hotspot[2];
};

struct message_type_cursor_position_t : public message_type_t
{	// The current mouse position. This is in floating point based on the relative mouse position.
	float posn[2];

	// Is the current cursor visible ?
	bool visible;
};

struct message_type_cursor_click_t : public message_type_t
{	
};	

struct message_type_wheel_t : public message_type_t
{	// How far to move ?
	float m_distance;
};

struct message_type_keyboard_t : public message_type_t
{	
	int key_code;  //the key code
	//interpret which kind of key code
	enum e_key_mapped_type
	{
		e_key_mapped_type_wm_key_up,
		e_key_mapped_type_wm_key_down
	} key_mapped_type;
};

struct message_type_clipboard_t : public message_type_t
{
	//Reserve a type specifier for future object types
	enum e_clipboard_data_type
	{
		e_clipboard_type_text,
		e_clipboard_type_html   //future
	} clipboard_data_type;
	//place csz (string) clipboard data here; must end with zero
};

struct message_type_touch_t : public message_type_t
{
	uint16_t size;  //this correlates to how many touch points have been detected at one time
	struct touch_input_element
	{
		float posn[2];
		//reserved to add anything more needed from TOUCHINPUT
	};
	//touch_input_element touch_input_data[x];
};

//Custom drive
struct message_type_drive_t : public message_type_t
{
	uint16_t size;
	//drive number eg:Z: place csz (string) clipboard data here; must end with zero
	//drive address eg:\\10.2.0.10\storage place csz (string) clipboard data here; must end with zero
	//password place csz (string) clipboard data here; must end with zero
	//user place csz (string) clipboard data here; must end with zero
};

//Custom peninfo
struct message_type_peninfo_t : public message_type_t
{
	float x;				// x coordinate, 0 ~ 1
	float y;				// y coordinate, 0 ~ 1
	unsigned int penMask;
	unsigned int penFlags;
	unsigned int pressure;
	unsigned int pointerFlags;
};
//Custom logout
struct message_type_logout_t : public message_type_t
{
};
#pragma pack(pop)
