﻿#ifndef MYOPENGLWIDGET_H
#define MYOPENGLWIDGET_H

#include<QOpenGLWidget>
#include<QOpenGLFunctions>
#include<QOpenGLBuffer>
#include<QMouseEvent>
#include<QTimer>
#include<QOpenGLTexture>
#include<QImage>
#include<chrono>


class QOpenGLShaderProgram;

class MyOpenglwIDGET : public QOpenGLWidget,protected QOpenGLFunctions
{
    Q_OBJECT
public:
    MyOpenglwIDGET(QWidget *parent=0);
protected:
    void initializeGL()override;
    void paintGL()override;
    void resizeGL(int width,int height)override;
    void mouseMoveEvent(QMouseEvent *event)override;
public slots:
    void setWidth(int width);
    void setHeight(int height);
private:
    QOpenGLShaderProgram *program;
    QOpenGLBuffer vbo;
    QVector2D mousePosition=QVector2D(0,0);
    //auto startTime =std::chrono::steady_clock::now();
    std::chrono::steady_clock::time_point startTime;
};

#endif // MYOPENGLWIDGET_H
