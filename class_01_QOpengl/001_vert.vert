#version 450 core

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexColor;
layout(location = 2) in vec2 vTexcoord;

out vec3 Color;
out vec2 Texcoord;

void main(void)
{
    gl_Position =  vec4(vertexPosition,1.0);
    Color = vertexColor;
    Texcoord = vTexcoord;
}