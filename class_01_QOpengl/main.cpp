﻿#include "widget.h"
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QWindow>
#include"myopenglwidget.h"
#include<thread>
#include<mutex>

std::mutex tex;
int main(int argc, char *argv[])
{

    QApplication::setAttribute(Qt::AA_DontCheckOpenGLContextThreadAffinity);

    QApplication app(argc, argv);
    QQmlApplicationEngine engine;

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    //将widget嵌入QML
    QObject *Qmlobj=engine.rootObjects().constFirst();
    QWindow *QmlWindow=qobject_cast<QWindow*>(Qmlobj);
    QmlWindow->setTitle("yyds");
    WId parent_HWND=QmlWindow->winId();
    MyOpenglwIDGET widget;
    widget.setProperty("_q_embedded_native_parent_handle",QVariant(parent_HWND));//给widget父对象句柄赋值
    widget.setGeometry(0,0,QmlWindow->width(),QmlWindow->height());
    widget.winId();
    widget.windowHandle()->setParent(QmlWindow);
    widget.show();


    QObject::connect(QmlWindow,SIGNAL(widthChanged(int)),&widget,SLOT(setWidth(int)));
    QObject::connect(QmlWindow,SIGNAL(heightChanged(int)),&widget,SLOT(setHeight(int)));

    return app.exec();
}


