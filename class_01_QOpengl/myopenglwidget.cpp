﻿#include "myopenglwidget.h"
#include <QOpenGLShader>
#include <QMetaObject>

MyOpenglwIDGET::MyOpenglwIDGET(QWidget *parent)
    :QOpenGLWidget(parent)
{
    this->setMouseTracking(true);
}

void MyOpenglwIDGET::initializeGL()
{
    startTime = std::chrono::steady_clock::now();
    initializeOpenGLFunctions();

    QOpenGLShader *verShader =new QOpenGLShader(QOpenGLShader::Vertex,this);
    const char *vsrc=
            "in vec4 vPosition;\n"           
            "void main(){\n"          
            "gl_Position = vPosition;\n"
            "}\n";

    verShader->compileSourceCode(vsrc);


    QOpenGLShader *fragShader =new QOpenGLShader(QOpenGLShader::Fragment,this);
    const char *fsrc =           
            "uniform vec2 u_resolution;\n"
            "uniform vec2 u_mousePosition;\n"
            "uniform float u_time;\n"
            "float random(in vec2 st){\n"
            "return fract(sin(dot(st.xy,vec2(12.9898,78.233)))\n"
            "*43758.5453121);\n"
            "}\n"
            "//*********************\n"
            "float doubleEllipticSigmoid (float x, float a, float b){\n"
            "float epsilon = 0.00001;\n"
            "float min_param_a = 0.0 + epsilon;\n"
            "float max_param_a = 1.0 - epsilon;\n"
            "float min_param_b = 0.0;\n"
            "float max_param_b = 1.0;\n"
            "a = max(min_param_a, min(max_param_a, a));\n"
            "b = max(min_param_b, min(max_param_b, b));\n"
            "float y = 0;\n"
            "if (x<=a){\n"
            "y = b * (1 - (sqrt(cos(a) - sin(x))/a));\n"
            "} else {\n"
            "y = b + ((1-b)/(1-a))*sqrt(cos(1-a) - tan(x-1));\n"
            "}\n"
            "return y;\n"
            "}\n"
            "//*******************\n"
            "float noise(in vec2 st){\n"
            "vec2 i=floor(st);\n"
            "vec2 f=fract(st);\n"
            "float a=random(i);\n"
            "float b=random(i+vec2(1.0,0.0));\n"
            "float c=random(i+vec2(0.0,1.0));\n"
            "float d=random(i+vec2(1.0,1.0));\n"
            "vec2 u=f*f*(3.0-2.0*f);\n"
            "return mix(a,b,u.x)+(c-a)*u.y*(1.0-u.x)+(d-b)*u.x*u.y;\n"
            "}\n"
            "//*********************//\n"
            "#define NUM_OCTAVES 5\n"
            "float fbm(in vec2 st){\n"
            "float v=0.0;\n"
            "float a=0.5;\n"
            "vec2 shift=vec2(100.0);\n"
            "mat2 rot=mat2(cos(0.5),sin(0.5),-sin(0.5),cos(0.50));\n"
            "for(int i=0;i<NUM_OCTAVES;++i){\n"
            "   v+=a*noise(st);\n"
            "   st=rot*st*2.0+shift;\n"
            "   a*=0.5;\n"
            "   }\n"
            "return v;\n"
            "}\n"
            "void main(){   \n"
            "vec2 st=gl_FragCoord.xy/u_resolution.xy*3.;\n"
            "st*=u_mousePosition/10;\n"
            "st.x = doubleEllipticSigmoid(st.y,0.2,0.189)*clamp(st.y,-1.0,1.0)*sin(u_time)*clamp(st.x,-1.0,1.0)*cos(u_time)*smoothstep(-1.0,1.0,st.y);\n"
            "st.y = doubleEllipticSigmoid(st.x,0.2,0.189);\n"
            "st.y = smoothstep(-1.0,1.0,st.x);\n"
            "st   = vec2(mix(st.y,st.x,st.x),mix(st.y,st.x,st.y));\n"
            "vec3 color = vec3(0.0);\n"
            "vec2 q = vec2(0.);\n"
            "q.x = fbm(st+0.00*u_time);\n"
            "q.y = fbm(st+vec2(1.0));\n"
            "vec2 r=vec2(0.);\n"
            "r.x = fbm(st+1.0*q+vec2(1.7,9.2)+0.15*u_time);\n"
            "r.y = fbm(st+1.0*q+vec2(8.3,2.8)+0.126*u_time);\n"
            "float f=fbm(st+r);\n"
            "color = mix(vec3(0.101961,0.619608,0.666667),vec3(0.666667,0.666667,0.498039),clamp((f*f)*4.0,0.0,1.0));\n"
            "color = mix(color,vec3(0,0,0.164706),clamp(length(q),0.0,0.5));\n"
            "color = mix(color,vec3(0.666667,1,1),clamp(length(r.x),0.0,0.5));\n"
            "gl_FragColor = dot(vec4((f*f*f+.6*f*f+.5*f)*color,sin(u_time)),vec4(color,cos(u_time)));\n"
            "}   \n";
    fragShader->compileSourceCode(fsrc);

    program =new QOpenGLShaderProgram;
    program->addShader(verShader);
    program->addShader(fragShader);

    program->link();
    program->bind();   
}

void MyOpenglwIDGET::paintGL()
{

    auto t2 = std::chrono::steady_clock::now();
    std::chrono::duration<float> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - startTime);
    GLfloat vertices[]=
    {
        -1.0,1.0,
        -1.0,-1.0,
        1.0,-1.0,
        1.0,1.0
    };
    vbo.create();
    vbo.bind();
    vbo.allocate(vertices,8*sizeof(GLfloat));

    GLuint vPosition = program->attributeLocation("vPosition");

    program->setAttributeArray(vPosition,GL_FLOAT,0,2,0);

    glEnableVertexAttribArray(vPosition);
    float endTime=time_span.count();
    QVector2D vec(this->width(),this->height());
    program->setUniformValue("u_resolution",vec);
    program->setUniformValue("u_mousePosition",mousePosition);   
    program->setUniformValue("u_time",endTime);

    glDrawArrays(GL_TRIANGLE_FAN,0,4);

    QMetaObject::invokeMethod(this,"update",Qt::QueuedConnection);
}

void MyOpenglwIDGET::resizeGL(int width, int height)
{
    this->resize(width,height);
}

void MyOpenglwIDGET::mouseMoveEvent(QMouseEvent *event)
{

    mousePosition=QVector2D(event->position().x(),event->position().y());

}

void MyOpenglwIDGET::setWidth(int width)
{
    this->setGeometry(0,0,width,this->height());
}

void MyOpenglwIDGET::setHeight(int height)
{
    this->setGeometry(0,0,this->width(),height);
}
