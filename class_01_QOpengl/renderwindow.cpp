﻿#include "renderwindow.h"

RenderWindow::RenderWindow(QQuickWidget *parent):QQuickWidget(parent)
{
    layout = new QGridLayout(this);

    widget = new MyOpenglwIDGET(this);
    layout->addWidget(widget);
}

int RenderWindow::m_getWidth()
{
    return this->width();
}

void RenderWindow::m_setWidth(int width)
{
    this->setFixedWidth(width);
    emit m_widthChanged(width);
}

int RenderWindow::m_getHeight()
{
    return this->height();
}

void RenderWindow::m_setHeight(int height)
{
    this->setFixedHeight(height);
    emit m_heightChanged(height);
}
