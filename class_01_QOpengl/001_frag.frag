#version 450 core

in vec3 Color;
in vec2 Texcoord;
out vec4 FragColor;

uniform sampler2D texturewall;
uniform sampler2D textureface;

void main(void)
{
    FragColor = mix(texture(texturewall, Texcoord),texture(textureface,Texcoord),0.3)*vec4(Color,1);
}

