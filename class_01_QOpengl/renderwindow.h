﻿#ifndef RENDERWINDOW_H
#define RENDERWINDOW_H

#include <QQuickItem>
#include <QQuickPaintedItem>
#include "myopenglwidget.h"
#include <QGridLayout>
#include <QQuickWidget>



class RenderWindow:public QQuickWidget
{
    Q_OBJECT
    Q_PROPERTY(int m_width READ m_getWidth WRITE m_setWidth NOTIFY m_widthChanged)
    Q_PROPERTY(int m_height READ m_getHeight WRITE m_setHeight NOTIFY m_heightChanged)
public:
    RenderWindow(QQuickWidget *parent=0);
    virtual ~RenderWindow(){}
private:
    int m_getWidth();
    void m_setWidth(int width);
    int m_getHeight();
    void m_setHeight(int width);
    QGridLayout *layout=0;
    MyOpenglwIDGET *widget;
    int m_width;
    int m_height;
signals:
    void m_widthChanged(int width);
    void m_heightChanged(int heihgt);
};

#endif // RENDERWINDOW_H
