#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "client.h"
#include "server.h"


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<Server, 1>("QServer", 1, 0, "QServer");
    qmlRegisterType<Client, 1>("QClient", 1, 0, "QClient");
    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/UDPStudent_001/main.qml"_qs);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
