import QtQuick
import QServer 1.0
import QClient 1.0

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")


    Rectangle
    {
        anchors.left: parent.left
        anchors.top:parent.top
        width: parent.width/2-20
        height: parent.height
        Server
        {
            id:qss2
            anchors.fill: parent
            onBtnStart:
            {
                ss2.start()
            }

            onBtnStop:
            {
                ss2.stop()
            }
        }
        QServer
        {
            id:ss2
            msg: qss2.cppMsg
        }
    }

    Rectangle
    {
        anchors.right: parent.right
        anchors.top:parent.top
        width: parent.width/2-20
        height: parent.height
        Client
        {
            id:qss
            anchors.fill: parent
            wenben: "dssdsd"
            onStart:
            {
                ss.bind()
            }

            onStop:
            {
                ss.close()
            }
        }
        QClient
        {
            id:ss
            onReciveData:function(msg)
            {
                qss.wenben += qsTr(msg)+"\n"
            }
        }
    }








}
