#include "server.h"
#include<QHostAddress>
#include<QDebug>

Server::Server(QObject *parent)
    : QObject{parent}
{
    port = 5555;
    udpSocket =new QUdpSocket(this);
    timer = new QTimer(this);

    connect(timer,&QTimer::timeout,this,&Server::timeout);
}

void Server::start()
{
    timer->start(100);
}

void Server::stop()
{
    timer->stop();
}

QString Server::getMsg()
{
    return m_msg;
}

void Server::setMsg(QString msg)
{
    m_msg = msg;
    emit msgChanged(msg);
}

void Server::timeout()
{
    int length = 0;

    if(m_msg == "")
    {
        return;
    }

    if((length=udpSocket->writeDatagram(m_msg.toLatin1(),m_msg.length(),QHostAddress::Broadcast,port))!=m_msg.length())
    {
        return;
    }
    qDebug()<<"已发送:"<<m_msg;
}
