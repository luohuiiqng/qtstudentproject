#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QUdpSocket>


class Client : public QObject
{
    Q_OBJECT

    //Q_PROPERTY(type name READ name WRITE setName NOTIFY nameChanged)
public:
    explicit Client(QObject *parent = nullptr);
public:
    Q_INVOKABLE void close();
    Q_INVOKABLE void bind();
    Q_INVOKABLE void listen();
    Q_INVOKABLE void sendMsg();
    void dataReceived();
signals:
    void reciveData(QString msg);

private:
    int port;
    QUdpSocket *udpSocket;

};

#endif // CLIENT_H
