﻿#include "client.h"
#include <QHostAddress>
//#include <QTextCodec>


Client::Client(QObject *parent)
    : QObject{parent}
{
    udpSocket = new QUdpSocket(this);
    port = 5555;

    connect(udpSocket,&QUdpSocket::readyRead,this,&Client::dataReceived);
}

void Client::close()
{
    udpSocket->abort();
}

void Client::bind()
{
    bool result = udpSocket->bind(port);
    if(!result)
    {
        qDebug()<<"bind fail!";
        return;
    }
}

void Client::listen()
{

}

void Client::sendMsg()
{

}

void Client::dataReceived()
{
    while(udpSocket->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(udpSocket->pendingDatagramSize());
        udpSocket->readDatagram(datagram.data(),datagram.size());
        QString msg =QString::fromLocal8Bit(datagram.toStdString());

        qDebug()<<"data:"<<msg;
        emit reciveData(msg);
    }
}
