import QtQuick
import QtQuick.Controls

Item {
    id:control
    implicitHeight: 430
    implicitWidth: 500
    property color backgroundColor: "black"
    property color textColor:"white"
    property string contentString: ""
    property bool isBroadCast: false
    property alias cppMsg: data.text
    signal btnStart();
    signal btnStop();

    Rectangle
    {
        anchors.fill: parent
        color:backgroundColor

        Rectangle
        {
            id:top
            anchors.top: parent.top
            anchors.left: parent.left
            height:60
            width:parent.width
            color:backgroundColor
            Text
            {
                anchors.fill: parent
                color:textColor
                text:qsTr("服务端")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

        }

        Rectangle
        {
            id:content
            anchors.top: top.bottom
            anchors.left: parent.left
            height:parent.height-90
            width:parent.width
            color:"#008B8B"
            TextEdit
            {
                id:data
                anchors.fill: parent
                wrapMode:TextEdit.Wrap
                focus: true
                color:textColor
                text:contentString
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
        }

        Rectangle
        {
            id:btn
            anchors.top: content.bottom
            anchors.left: parent.content
            height:30
            width:parent.width
            color:"gray"
            MouseArea
            {
                anchors.fill: parent
                onClicked:
                {

                    isBroadCast =!isBroadCast
                    if(isBroadCast)
                    {
                        btnStart()
                        btnBottom.text = "停止广播"
                        return
                    }

                    btnStop()
                    btnBottom.text = "开始广播"
                }

            }
            Text {
                id:btnBottom
                anchors.fill: parent
                color:textColor
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: qsTr("开始广播")
            }
        }
    }
}
