#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QUdpSocket>
#include <QTimer>


class Server : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString msg READ getMsg WRITE setMsg NOTIFY msgChanged)
public:
    explicit Server(QObject *parent = nullptr);
public:
    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();
private:
     QString getMsg();
     void setMsg(QString msg);
private slots:
    void timeout();

signals:
    void msgChanged(QString msg);
private:
    int port;
    bool isStartted;
    QUdpSocket *udpSocket;
    QTimer *timer;
    QString m_msg="";

};

#endif // SERVER_H
