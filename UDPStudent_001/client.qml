import QtQuick 2.0
import QtQuick .Controls 2.0

Item {
    id:control
    implicitHeight: 430
    implicitWidth: 500
    property color backgroundColor: "black"
    property color textColor:"white"
    property string contentString: ""
    property alias wenben: content.text
    property bool isBind: false
    signal start();
    signal stop();
    Rectangle
    {
        id:top
        anchors.top:parent.top
        anchors.left: parent.left
        height: 60
        width: parent.width
        color:backgroundColor
        Text {
            id: title
            anchors.fill: parent
            color:"white"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("客户端")
        }
    }

    Rectangle
    {
        id:body
        anchors.top: top.bottom
        anchors.left: parent.left
        height:parent.height-90
        width:parent.width
        color:"#008B8B"
        TextArea {
            id: content
            color:textColor
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            background: Rectangle
            {
                 color:"#008B8B"
                 anchors.fill:parent
            }

            text: ""
        }
    }

    Rectangle
    {
        id:btn
        anchors.top: body.bottom
        anchors.left: body.left
        height:30
        width:parent.width
        color:"gray"
        MouseArea
        {
            anchors.fill: parent
            onClicked:
            {

                isBind = !isBind
                if(isBind)
                {
                    start()
                    btnBottom.text = "解除绑定"
                    return
                }

                stop()
                btnBottom.text = "开始绑定"
            }

        }

        Text {
            id:btnBottom
            anchors.fill: parent
            color:textColor
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("开始绑定")
        }
    }
}
