#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QObject>
#include "server.h"

class TcpServer : public QObject
{
    Q_OBJECT
public:
    explicit TcpServer(){
        port = 8010;

    }
public slots:
    Q_INVOKABLE void slotCreateServer();
    void updateServer(QString,int);
signals:

private:
    int port;
    Server *server;

};

#endif // TCPSERVER_H
