#include "server.h"

Server::Server(QObject *parent,int port):QTcpServer(parent)
{
    listen(QHostAddress::Any,port);
}

void Server::updateClients(QString msg, int length)
{
    //通知服务器对话框更新相应的显示状态
    emit updateServer(msg,length);
    for(int i =0 ;i<tcpClientSocketList.count();i++)
    {
        //广播
        QTcpSocket *item = tcpClientSocketList.at(i);
        if(item->write(msg.toLatin1(),length)!=length)
        {
            continue;
        }
    }
}

void Server::slotDisconnected(int descriptor)
{
    for(int i =0;i<tcpClientSocketList.count();i++)
    {
        QTcpSocket *item = tcpClientSocketList.at(i);
        if(item->socketDescriptor() == descriptor)
        {
            tcpClientSocketList.removeAt(i);
            return;
        }
    }

    return;
}

void Server::incomingConnection(int socketDescriptor)
{
    //创建一个新的TcpClientSocket与客户端通信
    Tcpclientsocket *tcpClientSocket = new Tcpclientsocket(this);

    connect(tcpClientSocket,SIGNAL(updateClients(QString,int)),
            this,SLOT(updateClients(QString,int)));;
    connect(tcpClientSocket,SIGNAL(disconnected(int)),
            this,SLOT(slotDisconnected(int)));
    //设置套接字描述符
    tcpClientSocket->setSocketDescriptor(socketDescriptor);
    //将新的套接字加入客户端套接字列表加以管理
    tcpClientSocketList.append(tcpClientSocket);
}
