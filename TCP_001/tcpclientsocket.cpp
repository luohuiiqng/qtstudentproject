#include "tcpclientsocket.h"

Tcpclientsocket::Tcpclientsocket(QObject *parent)  
{
    //进行数据的读取
    connect(this,SIGNAL(readyRead()),this,SLOT(dataReceived()));
    //进行断开处理
    connect(this,SIGNAL(disconnected()),this,SLOT(slotDisconnected()));
}

void Tcpclientsocket::dataReceived()
{
    while(bytesAvailable()>0)
    {
        int length =bytesAvailable();
        char buf[1024];
        read(buf,length);
        QString msg = buf;
        emit updateClients(msg,length);
    }
}

void Tcpclientsocket::slotDisconnected()
{
    emit disconnected(this->socketDescriptor());
}
