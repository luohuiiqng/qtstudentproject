#ifndef TCPCLIENTSOCKET_H
#define TCPCLIENTSOCKET_H

#include <QObject>
#include <QTcpSocket>

class Tcpclientsocket : public QTcpSocket
{
    Q_OBJECT
public:
    explicit Tcpclientsocket(QObject *parent = nullptr);

signals:
    void updateClients(QString ,int);
    void disconnected(int);
protected slots:
    void dataReceived();
    void slotDisconnected();
};

#endif // TCPCLIENTSOCKET_H
