﻿#include "localhostinfo.h"

LocalHostInfo::LocalHostInfo()
{
    getHostInformation();
    slotDetail();
}

void LocalHostInfo::getHostInformation()
{
    this->m_localHostName =QHostInfo::localHostName();

    QHostInfo hostInfo =QHostInfo::fromName(this->m_localHostName);

    m_listAddress =hostInfo.addresses();
}

void LocalHostInfo::slotDetail()
{

    QList<QNetworkInterface>list =QNetworkInterface::allInterfaces();

    foreach(QNetworkInterface interface,list)
    {
        m_detail =m_detail+tr("设备: ")+interface.name()+"\n";
        m_detail =m_detail+tr("硬件地址: ")+interface.hardwareAddress()+"\n";

        QList<QNetworkAddressEntry>entryList =interface.addressEntries();
        foreach(QNetworkAddressEntry entryface,entryList)
        {
            m_detail =m_detail+"\t"+tr("IP地址:")+entryface.ip().toString()+"\n";
            m_detail =m_detail+"\t"+tr("子网掩码:")+entryface.netmask().toString()+"\n";
            m_detail =m_detail+"\t"+tr("广播地址:")+entryface.broadcast().toString()+"\n";
        }
    }

    //直接下标访问会越界，不知道为啥，用迭代器却不会！
    /*for(int i=0;i<list.count();i++)
    {
        QNetworkInterface interface=list[i];
        m_detail =m_detail+tr("设备: ")+interface.name()+"\n";
        m_detail =m_detail+tr("硬件地址: ")+interface.hardwareAddress()+"\n";
        QList<QNetworkAddressEntry>entryList =interface.addressEntries();

        for(int j=0;j<entryList.count();j++)
        {
            QNetworkAddressEntry entry =entryList.at(i);
            m_detail =m_detail+"\t"+tr("IP地址:")+entry.ip().toString()+"\n";
            m_detail =m_detail+"\t"+tr("子网掩码:")+entry.netmask().toString()+"\n";
            m_detail =m_detail+"\t"+tr("广播地址:")+entry.broadcast().toString()+"\n";
        }
    }*/


}

void LocalHostInfo::setLocalHostName(QString msg)
{
    this->m_localHostName=msg;
    emit localHostNameChanged(msg);
}

QString LocalHostInfo::getLocalHostName()
{
    return this->m_localHostName;
}

void LocalHostInfo::setDetail(QString msg)
{
    this->m_detail=msg;
    emit detailChanged(msg);
}

QString LocalHostInfo::getDetail()
{
    return this->m_detail;
}
