﻿#ifndef LOCALHOSTINFO_H
#define LOCALHOSTINFO_H
#include<QHostInfo>
#include<QNetworkInterface>
#include<QHostAddress>
#include<QList>


class LocalHostInfo:public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString localHostName READ getLocalHostName WRITE setLocalHostName NOTIFY localHostNameChanged)
    Q_PROPERTY(QString detail READ getDetail WRITE setDetail NOTIFY detailChanged);
public:
    LocalHostInfo();
    void getHostInformation();
    void slotDetail();
private:
    QString m_localHostName="";//本地主机名
    QString m_detail="";//网络详细信息
    QList<QHostAddress> m_listAddress ;//ip地址列表
private:
    void setLocalHostName(QString msg);
    QString getLocalHostName();
    void setDetail(QString msg);
    QString getDetail();
signals:
    void localHostNameChanged(QString msg);
    void detailChanged(QString msg);

};

#endif // LOCALHOSTINFO_H
