﻿import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls
import LocalHostInfo 1.0

Window {
    id:control
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    LocalHostInfo
    {
        id:netInfomation;
    }

    Rectangle
    {
        id:rec
        anchors.fill: parent
        color:"black"
        Row
        {
            anchors.fill: parent
            ScrollView
            {
                id:xview
                x:0
                y:30
                height: parent.height-30
                width: 450
                clip:true
                ScrollBar.vertical:ScrollBar
                {
                    parent:xview
                    anchors.right:parent.right
                    //anchors.rightMargin:120
                    anchors.top:parent.top
                    height:parent.height
                    active:true
                    width:30
                }
                TextArea
                {
                    text: "本地主机名: "+netInfomation.localHostName+"\n"+netInfomation.detail
                    color:"white"
                    background: Rectangle
                    {
                        color:"green"
                    }
                }
            }



        }
    }
}
