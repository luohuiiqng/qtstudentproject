import QtQuick
import QtQuick3D
import QtQuick.Timeline
Node {
    id: rootNode

    property alias erulationl: thigh_l.eulerRotation
    property alias erulationr: thigh_r.eulerRotation

    Node {
        id: sK_Mannequin
        Skeleton {
            id: qmlskeleton
            Joint {
                id: root
                index: 0
                skeletonRoot: qmlskeleton
                Joint {
                    id: pelvis
                    x: 1.35368e-28
                    y: 1.05615
                    z: 96.7506
                    rotation: Qt.quaternion(0.707106, 0.00129191, -0.707106, -0.00129191)
                    index: 1
                    skeletonRoot: qmlskeleton
                    Joint {
                        id: spine_01
                        x: 10.8089
                        y: -0.851415
                        z: -6.09109e-13
                        rotation: Qt.quaternion(0.998052, 0, 0, -0.0623886)
                        index: 2
                        skeletonRoot: qmlskeleton
                        Joint {
                            id: spine_02
                            x: 18.8753
                            y: 3.80116
                            z: 5.96609e-08
                            rotation: Qt.quaternion(0.992478, 0, 0, 0.12242)
                            index: 3
                            skeletonRoot: qmlskeleton
                            Joint {
                                id: spine_03
                                x: 13.4073
                                y: 0.420477
                                z: -5.57524e-13
                                rotation: Qt.quaternion(0.999706, 0, 0, 0.0242526)
                                index: 4
                                skeletonRoot: qmlskeleton
                                Joint {
                                    id: clavicle_l
                                    x: 11.8837
                                    y: -2.73209
                                    z: -3.78198
                                    rotation: Qt.quaternion(0.63966, 0.208918, 0.729417, 0.123044)
                                    index: 5
                                    skeletonRoot: qmlskeleton
                                    Joint {
                                        id: upperarm_l
                                        x: 15.7849
                                        y: 1.4318e-09
                                        z: 6.35913e-09
                                        rotation: Qt.quaternion(0.922963, 0.112996, 0.33063, -0.161419)
                                        scale.z: 1
                                        index: 6
                                        skeletonRoot: qmlskeleton
                                        Joint {
                                            id: lowerarm_l
                                            x: 30.3399
                                            y: 8.40747e-09
                                            z: 3.19753e-09
                                            rotation: Qt.quaternion(0.95991, -0.0540165, -0.0791827, -0.26341)
                                            index: 7
                                            skeletonRoot: qmlskeleton
                                            Joint {
                                                id: hand_l
                                                x: 26.9751
                                                y: 1.57297e-09
                                                z: -9.62075e-09
                                                rotation: Qt.quaternion(0.78595, -0.617895, 0.019372, 0.0106533)
                                                scale.x: 1
                                                index: 8
                                                skeletonRoot: qmlskeleton
                                                Joint {
                                                    id: index_01_l
                                                    x: 12.0681
                                                    y: 1.76346
                                                    z: -2.1094
                                                    rotation: Qt.quaternion(0.965614, 0.133305, -0.00318012, 0.22318)
                                                    scale.y: 1
                                                    scale.z: 1
                                                    index: 9
                                                    skeletonRoot: qmlskeleton
                                                    Joint {
                                                        id: index_02_l
                                                        x: 4.2875
                                                        y: -2.98503e-08
                                                        z: 5.04841e-09
                                                        rotation: Qt.quaternion(0.994453, 0.0120435, -0.00290585, 0.104448)
                                                        index: 10
                                                        skeletonRoot: qmlskeleton
                                                        Joint {
                                                            id: index_03_l
                                                            x: 3.39379
                                                            y: 1.16954e-08
                                                            z: -2.34923e-09
                                                            rotation: Qt.quaternion(0.996474, 0.0106111, 0.00785087, -0.0828552)
                                                            index: 11
                                                            skeletonRoot: qmlskeleton
                                                        }
                                                    }
                                                }
                                                Joint {
                                                    id: middle_01_l
                                                    x: 12.2443
                                                    y: 1.29364
                                                    z: 0.571162
                                                    rotation: Qt.quaternion(0.978037, 0.0285222, -0.0568739, 0.198485)
                                                    scale.x: 1
                                                    index: 12
                                                    skeletonRoot: qmlskeleton
                                                    Joint {
                                                        id: middle_02_l
                                                        x: 4.64037
                                                        y: -3.64818e-09
                                                        z: 1.83086e-09
                                                        rotation: Qt.quaternion(0.99404, -0.0186289, 0.00797217, 0.107117)
                                                        index: 13
                                                        skeletonRoot: qmlskeleton
                                                        Joint {
                                                            id: middle_03_l
                                                            x: 3.64884
                                                            y: -1.99894e-08
                                                            z: 1.60763e-09
                                                            rotation: Qt.quaternion(0.990268, 0.00162142, -0.038867, -0.133624)
                                                            index: 14
                                                            skeletonRoot: qmlskeleton
                                                        }
                                                    }
                                                }
                                                Joint {
                                                    id: pinky_01_l
                                                    x: 10.1407
                                                    y: 2.26315
                                                    z: 4.64315
                                                    rotation: Qt.quaternion(0.962869, -0.129538, -0.187897, 0.144213)
                                                    index: 15
                                                    skeletonRoot: qmlskeleton
                                                    Joint {
                                                        id: pinky_02_l
                                                        x: 3.57098
                                                        y: 1.86699e-08
                                                        z: 3.91818e-10
                                                        rotation: Qt.quaternion(0.995102, 0.0103597, -0.0105194, 0.0977483)
                                                        scale.x: 1
                                                        index: 16
                                                        skeletonRoot: qmlskeleton
                                                        Joint {
                                                            id: pinky_03_l
                                                            x: 2.98563
                                                            y: -3.00588e-08
                                                            z: -4.03754e-09
                                                            rotation: Qt.quaternion(0.999382, 0.00358095, 0.0337964, 0.0089303)
                                                            scale.x: 1
                                                            scale.z: 1
                                                            index: 17
                                                            skeletonRoot: qmlskeleton
                                                        }
                                                    }
                                                }
                                                Joint {
                                                    id: ring_01_l
                                                    x: 11.4979
                                                    y: 1.75353
                                                    z: 2.84691
                                                    rotation: Qt.quaternion(0.970419, -0.0954807, -0.116766, 0.188511)
                                                    index: 18
                                                    skeletonRoot: qmlskeleton
                                                    Joint {
                                                        id: ring_02_l
                                                        x: 4.43018
                                                        y: 4.6665e-09
                                                        z: -9.40334e-10
                                                        rotation: Qt.quaternion(0.993143, 0.00430111, -0.0141676, 0.115963)
                                                        scale.z: 1
                                                        index: 19
                                                        skeletonRoot: qmlskeleton
                                                        Joint {
                                                            id: ring_03_l
                                                            x: 3.47665
                                                            y: -1.67864e-08
                                                            z: 2.76865e-09
                                                            rotation: Qt.quaternion(0.993337, -0.000198824, 0.0262578, -0.112213)
                                                            scale.y: 1
                                                            scale.z: 1
                                                            index: 20
                                                            skeletonRoot: qmlskeleton
                                                        }
                                                    }
                                                }
                                                Joint {
                                                    id: thumb_01_l
                                                    x: 4.76204
                                                    y: 2.37498
                                                    z: -2.53782
                                                    rotation: Qt.quaternion(0.677278, 0.630309, 0.371525, -0.0772902)
                                                    scale.z: 1
                                                    index: 21
                                                    skeletonRoot: qmlskeleton
                                                    Joint {
                                                        id: thumb_02_l
                                                        x: 3.86967
                                                        y: 5.01187e-09
                                                        z: 9.98497e-09
                                                        rotation: Qt.quaternion(0.987685, 0.00260468, 0.0867984, 0.130141)
                                                        index: 22
                                                        skeletonRoot: qmlskeleton
                                                        Joint {
                                                            id: thumb_03_l
                                                            x: 4.06217
                                                            y: 1.07219e-09
                                                            z: -5.12728e-10
                                                            rotation: Qt.quaternion(0.993926, 0.0213991, 0.00188343, -0.107938)
                                                            index: 23
                                                            skeletonRoot: qmlskeleton
                                                        }
                                                    }
                                                }
                                            }
                                            Joint {
                                                id: lowerarm_twist_01_l
                                                x: 14
                                                y: 3.55271e-15
                                                z: 1.42109e-14
                                                index: 24
                                                skeletonRoot: qmlskeleton
                                            }
                                        }
                                        Joint {
                                            id: upperarm_twist_01_l
                                            x: 0.5
                                            y: -3.55271e-15
                                            index: 25
                                            skeletonRoot: qmlskeleton
                                        }
                                    }
                                }
                                Joint {
                                    id: clavicle_r
                                    x: 11.8838
                                    y: -2.7321
                                    z: 3.782
                                    rotation: Qt.quaternion(0.123044, 0.729417, -0.208918, -0.63966)
                                    index: 26
                                    skeletonRoot: qmlskeleton
                                    Joint {
                                        id: upperarm_r
                                        x: -15.7848
                                        y: -7.01396e-06
                                        z: -1.11715e-05
                                        rotation: Qt.quaternion(0.922963, 0.112996, 0.33063, -0.161419)
                                        scale.z: 1
                                        index: 27
                                        skeletonRoot: qmlskeleton
                                        Joint {
                                            id: lowerarm_r
                                            x: -30.34
                                            y: -4.08502e-06
                                            z: 1.75135e-06
                                            rotation: Qt.quaternion(0.95991, -0.0540165, -0.0791827, -0.26341)
                                            index: 28
                                            skeletonRoot: qmlskeleton
                                            Joint {
                                                id: hand_r
                                                x: -26.9752
                                                y: 2.56341e-05
                                                z: -1.19054e-06
                                                rotation: Qt.quaternion(0.78595, -0.617895, 0.019372, 0.0106533)
                                                scale.x: 1
                                                index: 29
                                                skeletonRoot: qmlskeleton
                                                Joint {
                                                    id: index_01_r
                                                    x: -12.0679
                                                    y: -1.76373
                                                    z: 2.10943
                                                    rotation: Qt.quaternion(0.965614, 0.133305, -0.00318012, 0.22318)
                                                    scale.y: 1
                                                    scale.z: 1
                                                    index: 30
                                                    skeletonRoot: qmlskeleton
                                                    Joint {
                                                        id: index_02_r
                                                        x: -4.28769
                                                        y: 9.24598e-05
                                                        z: -7.42621e-05
                                                        rotation: Qt.quaternion(0.994453, 0.0120435, -0.00290585, 0.104448)
                                                        index: 31
                                                        skeletonRoot: qmlskeleton
                                                        Joint {
                                                            id: index_03_r
                                                            x: -3.3938
                                                            y: 0.000120697
                                                            z: -1.2408e-05
                                                            rotation: Qt.quaternion(0.996474, 0.0106111, 0.00785087, -0.0828552)
                                                            index: 32
                                                            skeletonRoot: qmlskeleton
                                                        }
                                                    }
                                                }
                                                Joint {
                                                    id: middle_01_r
                                                    x: -12.2441
                                                    y: -1.29372
                                                    z: -0.57113
                                                    rotation: Qt.quaternion(0.978037, 0.0285222, -0.0568739, 0.198485)
                                                    scale.x: 1
                                                    index: 33
                                                    skeletonRoot: qmlskeleton
                                                    Joint {
                                                        id: middle_02_r
                                                        x: -4.64057
                                                        y: -0.000144911
                                                        z: 7.63696e-06
                                                        rotation: Qt.quaternion(0.99404, -0.0186289, 0.00797217, 0.107117)
                                                        index: 34
                                                        skeletonRoot: qmlskeleton
                                                        Joint {
                                                            id: middle_03_r
                                                            x: -3.64891
                                                            y: 3.29968e-05
                                                            z: -2.26664e-06
                                                            rotation: Qt.quaternion(0.990268, 0.00162142, -0.038867, -0.133624)
                                                            index: 35
                                                            skeletonRoot: qmlskeleton
                                                        }
                                                    }
                                                }
                                                Joint {
                                                    id: pinky_01_r
                                                    x: -10.1406
                                                    y: -2.26335
                                                    z: -4.64309
                                                    rotation: Qt.quaternion(0.962869, -0.129538, -0.187897, 0.144213)
                                                    index: 36
                                                    skeletonRoot: qmlskeleton
                                                    Joint {
                                                        id: pinky_02_r
                                                        x: -3.57106
                                                        y: -7.80197e-05
                                                        z: -8.10798e-06
                                                        rotation: Qt.quaternion(0.995102, 0.0103597, -0.0105194, 0.0977483)
                                                        scale.x: 1
                                                        index: 37
                                                        skeletonRoot: qmlskeleton
                                                        Joint {
                                                            id: pinky_03_r
                                                            x: -2.98542
                                                            y: 0.000317277
                                                            z: -3.50569e-05
                                                            rotation: Qt.quaternion(0.999382, 0.00358096, 0.0337964, 0.0089303)
                                                            scale.x: 1
                                                            scale.z: 1
                                                            index: 38
                                                            skeletonRoot: qmlskeleton
                                                        }
                                                    }
                                                }
                                                Joint {
                                                    id: ring_01_r
                                                    x: -11.498
                                                    y: -1.75377
                                                    z: -2.84691
                                                    rotation: Qt.quaternion(0.970419, -0.0954807, -0.116766, 0.188511)
                                                    index: 39
                                                    skeletonRoot: qmlskeleton
                                                    Joint {
                                                        id: ring_02_r
                                                        x: -4.42986
                                                        y: 8.44799e-05
                                                        z: -1.83787e-05
                                                        rotation: Qt.quaternion(0.993143, 0.00430111, -0.0141676, 0.115963)
                                                        scale.z: 1
                                                        index: 40
                                                        skeletonRoot: qmlskeleton
                                                        Joint {
                                                            id: ring_03_r
                                                            x: -3.47666
                                                            y: 7.19416e-05
                                                            z: -2.84313e-06
                                                            rotation: Qt.quaternion(0.993337, -0.000198824, 0.0262578, -0.112213)
                                                            scale.y: 1
                                                            scale.z: 1
                                                            index: 41
                                                            skeletonRoot: qmlskeleton
                                                        }
                                                    }
                                                }
                                                Joint {
                                                    id: thumb_01_r
                                                    x: -4.76212
                                                    y: -2.37512
                                                    z: 2.5378
                                                    rotation: Qt.quaternion(0.677278, 0.630309, 0.371525, -0.0772902)
                                                    scale.z: 1
                                                    index: 42
                                                    skeletonRoot: qmlskeleton
                                                    Joint {
                                                        id: thumb_02_r
                                                        x: -3.86957
                                                        y: 0.000113571
                                                        z: 5.59549e-05
                                                        rotation: Qt.quaternion(0.987685, 0.0026047, 0.0867984, 0.130141)
                                                        index: 43
                                                        skeletonRoot: qmlskeleton
                                                        Joint {
                                                            id: thumb_03_r
                                                            x: -4.06218
                                                            y: 2.01217e-06
                                                            z: 3.20496e-06
                                                            rotation: Qt.quaternion(0.993926, 0.0213991, 0.00188337, -0.107938)
                                                            index: 44
                                                            skeletonRoot: qmlskeleton
                                                        }
                                                    }
                                                }
                                            }
                                            Joint {
                                                id: lowerarm_twist_01_r
                                                x: -14
                                                y: 2.43319e-05
                                                z: -6.57833e-06
                                                rotation: Qt.quaternion(0.993058, -0.117627, 0, 0)
                                                index: 45
                                                skeletonRoot: qmlskeleton
                                            }
                                        }
                                        Joint {
                                            id: upperarm_twist_01_r
                                            x: -0.5
                                            y: -3.70079e-06
                                            z: -1.15598e-06
                                            rotation: Qt.quaternion(0.984881, -0.173235, 0, 0)
                                            index: 46
                                            skeletonRoot: qmlskeleton
                                        }
                                    }
                                }
                                Joint {
                                    id: neck_01
                                    x: 16.5588
                                    y: -0.355318
                                    z: -5.96597e-08
                                    rotation: Qt.quaternion(0.979031, 0, 0, -0.203711)
                                    index: 47
                                    skeletonRoot: qmlskeleton
                                    Joint {
                                        id: head
                                        x: 9.28361
                                        y: 0.364157
                                        z: 2.92737e-15
                                        rotation: Qt.quaternion(0.991043, 0, 0, 0.133542)
                                        scale.x: 1
                                        scale.y: 1
                                        index: 48
                                        skeletonRoot: qmlskeleton
                                    }
                                }
                            }
                        }
                    }
                    Joint {
                        id: thigh_l
                        x: -1.44883
                        y: -0.531424
                        z: -9.00581
                        rotation: Qt.quaternion(0.995305, 0.0737049, -0.0621388, -0.00858432)
                        index: 49
                        skeletonRoot: qmlskeleton
                        Joint {
                            id: calf_l
                            x: -42.572
                            y: 1.70742e-10
                            z: -4.66787e-10
                            rotation: Qt.quaternion(0.996474, -0.0488843, 0.0188639, -0.0655223)
                            index: 50
                            skeletonRoot: qmlskeleton
                            Joint {
                                id: calf_twist_01_l
                                x: -20.4768
                                rotation: Qt.quaternion(0.999965, 0.00280894, -0.00193338, -0.00761269)
                                scale.x: 1
                                scale.z: 1
                                index: 51
                                skeletonRoot: qmlskeleton
                            }
                            Joint {
                                id: foot_l
                                x: -40.1967
                                y: -3.93385e-09
                                z: 1.89946e-10
                                rotation: Qt.quaternion(0.996992, -0.00588577, 0.0319913, 0.0703548)
                                scale.x: 1
                                scale.y: 1
                                index: 52
                                skeletonRoot: qmlskeleton
                                Joint {
                                    id: ball_l
                                    x: -10.4538
                                    y: -16.5779
                                    z: 0.0801559
                                    rotation: Qt.quaternion(0.695389, 8.00888e-05, 2.96052e-05, -0.718634)
                                    index: 53
                                    skeletonRoot: qmlskeleton
                                }
                            }
                        }
                        Joint {
                            id: thigh_twist_01_l
                            x: -22.0942
                            y: 3.33067e-16
                            rotation: Qt.quaternion(0.998874, -0.0474436, 2.14764e-05, -0.000491109)
                            index: 54
                            skeletonRoot: qmlskeleton
                        }
                    }
                    Joint {
                        id: thigh_r
                        x: -1.44864
                        y: -0.531428
                        z: 9.0058
                        rotation: Qt.quaternion(0.00858438, 0.0621388, 0.0737049, 0.995305)
                        index: 55
                        skeletonRoot: qmlskeleton
                        Joint {
                            id: calf_r
                            x: 42.5723
                            y: -1.62336e-06
                            z: -5.83676e-07
                            rotation: Qt.quaternion(0.996474, -0.0488843, 0.0188639, -0.0655223)
                            index: 56
                            skeletonRoot: qmlskeleton
                            Joint {
                                id: calf_twist_01_r
                                x: 20.4769
                                z: -3.55271e-15
                                rotation: Qt.quaternion(0.999965, 0.00280728, -0.00193373, -0.00761253)
                                scale.x: 1
                                index: 57
                                skeletonRoot: qmlskeleton
                            }
                            Joint {
                                id: foot_r
                                x: 40.1968
                                y: 1.67694e-06
                                z: -1.0918e-05
                                rotation: Qt.quaternion(0.996992, -0.00588577, 0.0319913, 0.0703548)
                                scale.x: 1
                                scale.y: 1
                                index: 58
                                skeletonRoot: qmlskeleton
                                Joint {
                                    id: ball_r
                                    x: 10.4538
                                    y: 16.5778
                                    z: -0.0801584
                                    rotation: Qt.quaternion(0.695389, 8.00888e-05, 2.96052e-05, -0.718634)
                                    index: 59
                                    skeletonRoot: qmlskeleton
                                }
                            }
                        }
                        Joint {
                            id: thigh_twist_01_r
                            x: 22.0942
                            y: 2.22045e-16
                            z: -3.55271e-15
                            rotation: Qt.quaternion(0.998874, -0.0474453, 2.18983e-05, -0.000491179)
                            index: 60
                            skeletonRoot: qmlskeleton
                        }
                    }
                }
                Joint {
                    id: ik_foot_root
                    index: 61
                    skeletonRoot: qmlskeleton
                    Joint {
                        id: ik_foot_l
                        x: 17.0763
                        y: 8.07213
                        z: 13.4657
                        rotation: Qt.quaternion(0.701544, 0.0205303, -0.712233, 0.0117235)
                        scale.z: 1
                        index: 62
                        skeletonRoot: qmlskeleton
                    }
                    Joint {
                        id: ik_foot_r
                        x: -17.0763
                        y: 8.07215
                        z: 13.4656
                        rotation: Qt.quaternion(0.0205311, -0.701544, 0.0117243, 0.712233)
                        index: 63
                        skeletonRoot: qmlskeleton
                    }
                }
                Joint {
                    id: ik_hand_root
                    index: 64
                    skeletonRoot: qmlskeleton
                    Joint {
                        id: ik_hand_gun
                        x: -56.6461
                        y: 0.335412
                        z: 111.68
                        rotation: Qt.quaternion(0.678795, 0.618849, -0.0695411, 0.389136)
                        index: 65
                        skeletonRoot: qmlskeleton
                        Joint {
                            id: ik_hand_l
                            x: 77.8854
                            y: -69.6019
                            z: 43.8695
                            rotation: Qt.quaternion(-3.79317e-07, 0.687474, -0.614358, 0.387225)
                            scale.y: 1
                            index: 66
                            skeletonRoot: qmlskeleton
                        }
                        Joint {
                            id: ik_hand_r
                            index: 67
                            skeletonRoot: qmlskeleton
                        }
                    }
                }
            }
        }
        Model {
            id: sK_Mannequin_1
            skeleton: qmlskeleton
            inverseBindPoses: [
                Qt.matrix4x4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
                Qt.matrix4x4(1.20184e-07, -0.00365412, 0.999993, -96.7461, 2.19584e-10, 0.999993, 0.00365412, -1.40968, -1, -2.19584e-10, 1.20184e-07, -1.16276e-05, 0, 0, 0, 1),
                Qt.matrix4x4(1.19221e-07, -0.128159, 0.991754, -106.648, 1.51849e-08, 0.991754, 0.128159, -13.9482, -1, -2.19584e-10, 1.20184e-07, -1.16276e-05, 0, 0, 0, 1),
                Qt.matrix4x4(1.19338e-07, 0.116677, 0.99317, -126.074, -1.42407e-08, 0.99317, -0.116677, 13.2846, -1, -2.19584e-10, 1.20184e-07, -1.16873e-05, 0, 0, 0, 1),
                Qt.matrix4x4(1.18507e-07, 0.164699, 0.986344, -138.694, -2.00108e-08, 0.986344, -0.164699, 19.6126, -1, -2.19586e-10, 1.20184e-07, -1.16873e-05, 0, 0, 0, 1),
                Qt.matrix4x4(0.881745, 0.440334, -0.169211, 21.2038, -0.446774, 0.894647, 1.66222e-05, -0.782413, 0.151391, 0.0755842, 0.98558, -150.788, 0, 0, 0, 1),
                Qt.matrix4x4(0.64484, 0.0724998, -0.760872, 101.655, -0.0680491, 0.996984, 0.037326, -14.0586, 0.761283, 0.0277074, 0.647828, -110.614, 0, 0, 0, 1),
                Qt.matrix4x4(0.718491, -0.429115, -0.547385, 47.5512, 0.226213, 0.888381, -0.39951, 31.5053, 0.657722, 0.163218, 0.735365, -119.437, 0, 0, 0, 1),
                Qt.matrix4x4(0.687474, -0.442205, -0.57605, 25.5386, -0.614358, 0.0688189, -0.786021, 122.56, 0.387226, 0.89427, -0.224361, 2.82203, 0, 0, 0, 1),
                Qt.matrix4x4(0.38012, -0.309839, -0.871498, 64.4144, -0.729075, 0.479441, -0.488453, 99.9154, 0.569173, 0.821058, -0.0436507, -25.7948, 0, 0, 0, 1),
                Qt.matrix4x4(0.225136, -0.196698, -0.954266, 79.3492, -0.77866, 0.552396, -0.297568, 84.6093, 0.585664, 0.810042, -0.0287964, -28.4371, 0, 0, 0, 1),
                Qt.matrix4x4(0.340271, -0.299194, -0.891459, 61.441, -0.718957, 0.528251, -0.45172, 95.4191, 0.606066, 0.794628, -0.0353593, -29.2716, 0, 0, 0, 1),
                Qt.matrix4x4(0.43979, -0.268394, -0.857058, 59.1252, -0.821233, 0.266108, -0.504741, 106.385, 0.36354, 0.925825, -0.103382, -8.59933, 0, 0, 0, 1),
                Qt.matrix4x4(0.247786, -0.223979, -0.942569, 76.0219, -0.908447, 0.284346, -0.306384, 92.5542, 0.336639, 0.932192, -0.133016, -3.82445, 0, 0, 0, 1),
                Qt.matrix4x4(0.504489, -0.219237, -0.83512, 44.7714, -0.805879, 0.227619, -0.546579, 108.341, 0.30992, 0.948749, -0.0618471, -8.75118, 0, 0, 0, 1),
                Qt.matrix4x4(0.535458, -0.0799543, -0.840769, 52.3439, -0.843224, -0.106617, -0.526883, 108.283, -0.0475136, 0.99108, -0.124508, 15.7111, 0, 0, 0, 1),
                Qt.matrix4x4(0.36016, -0.0763704, -0.929759, 69.2324, -0.932096, -0.0705891, -0.355267, 96.9829, -0.038499, 0.994578, -0.096608, 12.3267, 0, 0, 0, 1),
                Qt.matrix4x4(0.345015, -0.144582, -0.927395, 67.0076, -0.938564, -0.0615124, -0.33958, 95.8941, -0.00794905, 0.987579, -0.156922, 16.1419, 0, 0, 0, 1),
                Qt.matrix4x4(0.455208, -0.201535, -0.867277, 59.5485, -0.884494, 0.00951435, -0.466455, 105.2, 0.102259, 0.979435, -0.173925, 13.3575, 0, 0, 0, 1),
                Qt.matrix4x4(0.242139, -0.165304, -0.956056, 78.2215, -0.965041, 0.0608527, -0.254936, 89.7342, 0.100321, 0.984363, -0.144791, 10.6112, 0, 0, 0, 1),
                Qt.matrix4x4(0.445625, -0.225786, -0.866279, 52.201, -0.887391, 0.016281, -0.46073, 104.07, 0.11813, 0.974041, -0.193105, 14.0056, 0, 0, 0, 1),
                Qt.matrix4x4(0.0334591, -0.826994, -0.561214, 55.2794, 0.583462, 0.472071, -0.660849, 39.4269, 0.811451, -0.305336, 0.498315, -101.462, 0, 0, 0, 1),
                Qt.matrix4x4(0.0434998, -0.612802, -0.789038, 76.3752, 0.577612, 0.659832, -0.480611, 22.0835, 0.815152, -0.434851, 0.382664, -90.3948, 0, 0, 0, 1),
                Qt.matrix4x4(-0.0882196, -0.736405, -0.670765, 66.647, 0.607304, 0.493997, -0.622212, 33.2619, 0.789556, -0.46225, 0.403642, -91.3231, 0, 0, 0, 1),
                Qt.matrix4x4(0.718491, -0.429115, -0.547385, 33.5512, 0.226213, 0.888381, -0.39951, 31.5053, 0.657722, 0.163218, 0.735365, -119.437, 0, 0, 0, 1),
                Qt.matrix4x4(0.64484, 0.0724998, -0.760872, 101.155, -0.0680491, 0.996984, 0.037326, -14.0586, 0.761283, 0.0277074, 0.647828, -110.614, 0, 0, 0, 1),
                Qt.matrix4x4(0.881745, -0.440333, 0.169211, -21.2038, -0.446774, -0.894647, -1.60336e-05, 0.782324, 0.151391, -0.0755847, -0.98558, 150.788, 0, 0, 0, 1),
                Qt.matrix4x4(0.64484, -0.0724994, 0.760871, -101.656, -0.0680491, -0.996984, -0.0373255, 14.0585, 0.761282, -0.0277077, -0.647828, 110.614, 0, 0, 0, 1),
                Qt.matrix4x4(0.718491, 0.429115, 0.547385, -47.5511, 0.226213, -0.88838, 0.39951, -31.5053, 0.657722, -0.163219, -0.735365, 119.437, 0, 0, 0, 1),
                Qt.matrix4x4(0.687474, 0.442206, 0.57605, -25.5385, -0.614358, -0.0688184, 0.786021, -122.56, 0.387226, -0.89427, 0.224362, -2.82212, 0, 0, 0, 1),
                Qt.matrix4x4(0.38012, 0.309839, 0.871498, -64.4143, -0.729075, -0.47944, 0.488453, -99.9152, 0.569173, -0.821058, 0.0436509, 25.7946, 0, 0, 0, 1),
                Qt.matrix4x4(0.225136, 0.196698, 0.954266, -79.349, -0.77866, -0.552396, 0.297569, -84.6093, 0.585664, -0.810042, 0.0287967, 28.437, 0, 0, 0, 1),
                Qt.matrix4x4(0.340271, 0.299195, 0.891458, -61.4408, -0.718957, -0.528251, 0.451721, -95.4191, 0.606066, -0.794628, 0.0353595, 29.2716, 0, 0, 0, 1),
                Qt.matrix4x4(0.43979, 0.268394, 0.857058, -59.1252, -0.821233, -0.266108, 0.504741, -106.385, 0.36354, -0.925824, 0.103383, 8.59921, 0, 0, 0, 1),
                Qt.matrix4x4(0.247787, 0.223979, 0.942568, -76.0217, -0.908447, -0.284345, 0.306385, -92.5541, 0.336639, -0.932191, 0.133017, 3.82433, 0, 0, 0, 1),
                Qt.matrix4x4(0.504489, 0.219237, 0.83512, -44.7711, -0.805879, -0.227619, 0.54658, -108.341, 0.30992, -0.948749, 0.0618475, 8.75104, 0, 0, 0, 1),
                Qt.matrix4x4(0.535458, 0.0799548, 0.840769, -52.3438, -0.843224, 0.106617, 0.526883, -108.282, -0.0475136, -0.99108, 0.124509, -15.7112, 0, 0, 0, 1),
                Qt.matrix4x4(0.36016, 0.0763709, 0.929759, -69.2322, -0.932096, 0.0705893, 0.355267, -96.9827, -0.038499, -0.994578, 0.0966085, -12.3268, 0, 0, 0, 1),
                Qt.matrix4x4(0.345015, 0.144582, 0.927394, -67.0076, -0.938563, 0.0615125, 0.33958, -95.8943, -0.00794899, -0.987579, 0.156923, -16.142, 0, 0, 0, 1),
                Qt.matrix4x4(0.455208, 0.201535, 0.867277, -59.5483, -0.884494, -0.0095141, 0.466455, -105.2, 0.102259, -0.979435, 0.173926, -13.3577, 0, 0, 0, 1),
                Qt.matrix4x4(0.242139, 0.165305, 0.956056, -78.2216, -0.965041, -0.0608526, 0.254936, -89.7342, 0.100321, -0.984363, 0.144791, -10.6113, 0, 0, 0, 1),
                Qt.matrix4x4(0.445626, 0.225786, 0.866278, -52.201, -0.887391, -0.0162807, 0.46073, -104.07, 0.11813, -0.974041, 0.193106, -14.0057, 0, 0, 0, 1),
                Qt.matrix4x4(0.0334592, 0.826994, 0.561214, -55.2792, 0.583463, -0.47207, 0.660849, -39.4268, 0.81145, 0.305336, -0.498315, 101.462, 0, 0, 0, 1),
                Qt.matrix4x4(0.0435001, 0.612803, 0.789038, -76.375, 0.577612, -0.659832, 0.480611, -22.0836, 0.815152, 0.434851, -0.382665, 90.3948, 0, 0, 0, 1),
                Qt.matrix4x4(-0.0882194, 0.736405, 0.670764, -66.6468, 0.607305, -0.493997, 0.622212, -33.2619, 0.789556, 0.462249, -0.403642, 91.3231, 0, 0, 0, 1),
                Qt.matrix4x4(0.718491, 0.429115, 0.547385, -33.5511, 0.066295, -0.825665, 0.560251, -58.5367, 0.692369, -0.366247, -0.621682, 108.772, 0, 0, 0, 1),
                Qt.matrix4x4(0.64484, -0.0724994, 0.760871, -101.156, -0.323738, -0.927689, 0.185974, -24.5302, 0.692369, -0.366247, -0.621682, 108.772, 0, 0, 0, 1),
                Qt.matrix4x4(1.16653e-07, -0.242401, 0.970176, -150.332, 2.89197e-08, 0.970176, 0.242401, -43.6161, -1, -2.19584e-10, 1.20184e-07, -1.16276e-05, 0, 0, 0, 1),
                Qt.matrix4x4(1.20147e-07, 0.0230424, 0.999735, -165.564, -2.98886e-09, 0.999735, -0.0230424, -0.162681, -1, -2.19584e-10, 1.20184e-07, -1.16276e-05, 0, 0, 0, 1),
                Qt.matrix4x4(-0.122429, -0.0298731, 0.992028, -93.4217, -0.147785, 0.988952, 0.011542, -0.293202, -0.981413, -0.145193, -0.125491, 20.8746, 0, 0, 0, 1),
                Qt.matrix4x4(-0.0711106, -0.156031, 0.985189, -50.9891, -0.0635316, 0.986393, 0.151636, -8.92087, -0.995443, -0.0518078, -0.0800558, 18.4947, 0, 0, 0, 1),
                Qt.matrix4x4(-0.0739403, -0.171238, 0.982451, -30.3019, -0.0702265, 0.983596, 0.166152, -9.27947, -0.994787, -0.0567088, -0.0847528, 18.6634, 0, 0, 0, 1),
                Qt.matrix4x4(-0.0148259, -0.0128132, 0.999808, -13.1067, -0.045694, 0.998882, 0.0121237, -7.44766, -0.998846, -0.0455055, -0.0153949, 17.6312, 0, 0, 0, 1),
                Qt.matrix4x4(0.0463127, -0.997914, -0.0449773, -9.04081, -0.0133848, -0.0456415, 0.998868, -2.95027, -0.998837, -0.0456583, -0.0154706, 17.5499, 0, 0, 0, 1),
                Qt.matrix4x4(-0.122287, -0.0308459, 0.992015, -71.327, -0.0542205, 0.998232, 0.0243554, -2.34022, -0.991013, -0.0508093, -0.123743, 20.7465, 0, 0, 0, 1),
                Qt.matrix4x4(-0.122429, 0.0298731, -0.992028, 93.4219, -0.147785, -0.988952, -0.011542, 0.2932, -0.981413, 0.145193, 0.125491, -20.8747, 0, 0, 0, 1),
                Qt.matrix4x4(-0.0711108, 0.156031, -0.985189, 50.989, -0.0635317, -0.986393, -0.151636, 8.92087, -0.995443, 0.0518078, 0.0800561, -18.4948, 0, 0, 0, 1),
                Qt.matrix4x4(-0.0739412, 0.171238, -0.982451, 30.3017, -0.0702232, -0.983597, -0.166152, 9.27952, -0.994787, 0.0567054, 0.0847532, -18.6635, 0, 0, 0, 1),
                Qt.matrix4x4(-0.0148262, 0.0128132, -0.999808, 13.1065, -0.045694, -0.998882, -0.0121237, 7.44768, -0.998846, 0.0455055, 0.0153951, -17.6313, 0, 0, 0, 1),
                Qt.matrix4x4(0.0463127, 0.997914, 0.0449773, 9.04074, -0.013385, 0.0456415, -0.998868, 2.95013, -0.998837, 0.0456583, 0.0154709, -17.5499, 0, 0, 0, 1),
                Qt.matrix4x4(-0.122286, 0.030846, -0.992015, 71.3272, -0.0542173, -0.998232, -0.0243559, 2.3403, -0.991013, 0.050806, 0.123743, -20.7465, 0, 0, 0, 1),
                Qt.matrix4x4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
                Qt.matrix4x4(-0.0148276, -0.0127956, 0.999808, -13.1067, -0.0456939, 0.998882, 0.0121061, -7.44584, -0.998845, -0.0455056, -0.0153957, 17.6312, 0, 0, 0, 1),
                Qt.matrix4x4(-0.0148276, 0.0127956, -0.999808, 13.1065, -0.0456959, -0.998882, -0.0121061, 7.44582, -0.998845, 0.0455077, 0.0153958, -17.6312, 0, 0, 0, 1),
                Qt.matrix4x4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
                Qt.matrix4x4(0.687474, 0.442216, 0.576042, -25.5377, -0.614358, -0.0688033, 0.786022, -122.561, 0.387225, -0.894266, 0.224379, -2.82379, 0, 0, 0, 1),
                Qt.matrix4x4(0.687474, -0.442216, -0.576042, 25.5378, -0.614358, 0.0688041, -0.786022, 122.561, 0.387226, 0.894266, -0.224378, 2.82367, 0, 0, 0, 1),
                Qt.matrix4x4(0.687474, 0.442216, 0.576042, -25.5377, -0.614358, -0.0688033, 0.786022, -122.561, 0.387225, -0.894266, 0.224379, -2.82379, 0, 0, 0, 1)
            ]
            source: "qrc:/model/meshes/sK_Mannequin.mesh"

            DefaultMaterial {
                id: m_Male_Body_material
                diffuseColor: "pink"
            }
            materials: [
                m_Male_Body_material,
                m_Male_Body_material
            ]
        }
    }

}
