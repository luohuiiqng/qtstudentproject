import QtQuick
import QtQuick.Controls

Item {

    implicitHeight: 30
    implicitWidth: 100
    property color backgroundColor: "black"
    property color contentColor: "blue"
    property alias value: slider.value
    property alias label: text.text
    property alias to: slider.to
    property alias from: slider.from

    Row
    {
        anchors.fill: parent
        Slider
        {
            id:slider
            height: parent.height
            width: parent.width-20
            from:0
            to:360
            value:0
        }
        Text {
            id: text
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: ""
        }
    }



}
