import QtQuick
import QtQuick3D
import QtQuick.Controls

ApplicationWindow {
    id:root
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    property bool isMove: false
    property bool isIgnore: false




    Rectangle
    {
        width: 160
        height: 700
        anchors.top: parent.top
        anchors.left: parent.left
        color:"pink"
        opacity: 0.5
        z:1000

        Keys.enabled: true
        focus: true


        MouseArea
        {
            anchors.fill: parent

            onClicked:
            {
                move.running = true
                sliderFour.value+=1
            }

            onPressed:
            {
                move.running = true
                sliderFour.value+=1
            }

            onMouseXChanged:
            {
                move.resume()
                sliderSix.value+=1
            }

            onReleased:
            {
                move.pause()
            }
        }

        Keys.onUpPressed:
        {
            isIgnore = true
            if(isIgnore)
            {
                return
            }

            move.start()
            move.running = true
            sliderFour.value+=1
            isIgnore = true
        }
        Keys.onDownPressed:
        {
            if(isIgnore)
            {
                return
            }
            root.isMove = true
            move.running = true
            sliderFour.value-=1
            isIgnore = true
        }


        Keys.onLeftPressed:
        {
            if(isIgnore)
            {
                return
            }
            root.isMove = true
            move.running = true
            sliderFive.value-=1
            isIgnore = true
        }

        Keys.onRightPressed:
        {
            if(isIgnore)
            {
                return
            }
            root.isMove = true
            move.running = true
            sliderFive.value-=1
            isIgnore = true
        }

        Keys.onReleased:
        {
            move.running = false
            isIgnore = false
        }

        Column
        {
            anchors.fill: parent
            XSlider
            {
                id:sliderOne
                label: "x方向旋转"+value.toFixed(2)
                value:237
            }
            XSlider
            {
                id:sliderTwo
                label: "y方向旋转"+value.toFixed(2)
            }
            XSlider
            {
                id:sliderThree
                label: "z方向旋转"+value.toFixed(2)
            }

            XSlider
            {
                id:sliderFour
                label: "x方向移动"+value.toFixed(2)
                from:-1000
                to:1000
            }
            XSlider
            {
                id:sliderFive
                label: "y方向移动"+value.toFixed(2)
                from:-1000
                to:1000
                value:-87
            }
            XSlider
            {
                id:sliderSix
                label: "z方向移动"+value.toFixed(2)
                from:-1000
                to:1000
            }

            XSlider
            {
                id:sliderSeven
                label: "x方向缩放"+value.toFixed(2)
                from:1
                to:10
            }
            XSlider
            {
                id:sliderEight
                label: "y方向缩放"+value.toFixed(2)
                from:1
                to:10
            }
            XSlider
            {
                id:sliderNine
                label: "z方向缩放"+value.toFixed(2)
                from:1
                to:10
            }
            Text {
                id: lightLabel
                text: qsTr("灯光位置:")
            }


            XSlider
            {
                id:slider_01
                label: "x方向"+value.toFixed(2)
                value: 50
                from:-1000
                to:1000
            }
            XSlider
            {
                id:slider_02
                label: "y方向"+value.toFixed(2)
                value: 200
                from:-1000
                to:1000
            }
            XSlider
            {
                id:slider_03
                label: "z方向"+value.toFixed(2)
                value: 50
                from:-1000
                to:1000
            }
            Text {
                id: cameraLabel
                text: qsTr("摄像机位置:")
            }
            XSlider
            {
                id:slider_04
                label: "x方向"+value.toFixed(2)
                value: 0
                from:-1000
                to:1000
            }
            XSlider
            {
                id:slider_05
                label: "y方向"+value.toFixed(2)
                value: 200
                from:-1000
                to:1000
            }
            XSlider
            {
                id:slider_06
                label: "z方向"+value.toFixed(2)
                value: 300
                from:-1000
                to:1000
            }

            Text
            {
                text: qsTr("摄像机焦距:")
            }
            XSlider
            {
                id:slider_07
                label: "焦距"+value.toFixed(2)
                value: 50
                from:0
                to:100
            }

        }
    }

    View3D
    {
        anchors.fill: parent

        environment: SceneEnvironment
        {
            clearColor:"#222222"
            backgroundMode:SceneEnvironment.Color
        }

        SK_Mannequin
        {
            id:human
            eulerRotation.x:sliderOne.value
            eulerRotation.y:sliderTwo.value
            eulerRotation.z:sliderThree.value
            position: Qt.vector3d(sliderFour.value,sliderFive.value,sliderSix.value)
            scale: Qt.vector3d(sliderSeven.value,sliderEight.value,sliderNine.value)

        }

        Cj
        {
            id:rec
        }

        Model
        {
            position: Qt.vector3d(0,-100,0)
            source: "#Cube"
            eulerRotation.x:30
            scale: Qt.vector3d(100,1,100)
            materials: [PrincipledMaterial{baseColor:"black";}]
        }

        PointLight
        {
            id:light
            position:Qt.vector3d(slider_01.value,slider_02.value,slider_03.value)
            eulerRotation.x:-90
            brightness: 100
            ambientColor: Qt.rgba(0.1,0.1,0.1,0.1)
            castsShadow: true
        }

        PerspectiveCamera
        {
            position: Qt.vector3d(slider_04.value,slider_05.value,slider_06.value)
            fieldOfView:slider_07.value
            Component.onCompleted: lookAt(Qt.vector3d(0,0,0))
        }


    }

    ParallelAnimation {
        id:move
        NumberAnimation {
            target: human
            property: "erulationl.z"
            duration: 1000
            from: -30
            to: 60
            easing.type: Easing.InOutQuad
        }
        NumberAnimation {
            target: human
            property: "erulationr.z"
            duration: 1000
            from: -160
            to: -120
            easing.type: Easing.InOutQuad
        }
        loops: Animation.Infinite
        running: true
    }




}


