#include "keys.h"
#include <QDebug>

KeyEvent::KeyEvent(QObject* parent) : QObject(parent)
{
    // 定时器
    m_pTimer = new QTimer(this);
    connect(m_pTimer, &QTimer::timeout, [ = ]
    {
        m_nClickCnt = 0; // 计数清零
        m_pTimer->stop(); // 停止计时
        qDebug() << "键盘单击";
    });
}

void KeyEvent::keyReleaseEvent(QKeyEvent* event)
{
    Q_UNUSED(event);
    if(event->key() == Qt::Key_A)   // Qt::Key_Control经实测，长按永远不会使isAutoRepeat()为true
    {
        // 是否是长按可以从release中直接判断
        if(!event->isAutoRepeat())
        {
            // LongPress_初始值为false，如果非长按执行单击或双击动作判断
            // 如果长按会在长按里将其置true，在最后的Relese(非长按)里就不会执行单击、双击判断的动作
            if(!m_bLongPress)
            {
                if(!m_pTimer->isActive())
                {
                    m_pTimer->start(500);
                }
                m_nClickCnt++;
                if(m_nClickCnt == 2)
                {
                    m_nClickCnt = 0; // 计数清零
                    m_pTimer->stop(); // 停止计时
                    qDebug() << "键盘双击";
                }
            }
            m_bLongPress = false; // 置false
        }
        else
        {
            if(!m_bLongPress)
            {
                qDebug() << "键盘长按";
                // 限定长按只执行一次,最后会在Relese(非长按)里将LongPress_重新置false
                m_bLongPress = true;
            }
        }
    }
}

bool KeyEvent::eventFilter(QObject* obj, QEvent* ev)
{
    qDebug() << "1212";
    return true;
}

