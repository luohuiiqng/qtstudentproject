#ifndef KEYS_H
#define KEYS_H


#include <QObject>
#include <QKeyEvent>
#include <QTimer>
#include <QDebug>
#include<QEvent>

class KeyEvent : public QObject
{
    Q_OBJECT

public:
    KeyEvent(QObject* parent = nullptr);

protected:
    void keyReleaseEvent(QKeyEvent* event);
public:
    Q_INVOKABLE bool eventFilter(QObject* obj, QEvent* ev) override;

private:
    QTimer* m_pTimer; // Ctrl双击定时器
    int m_nClickCnt = 0; // 点击次数
    bool m_bLongPress = false; // 是否为长按
};

#endif // KEYS_H
