import QtQuick
import QtQuick3D
Node {
    id: rootNode
    Model {
        id: sM_flower_01
        x: 164.724
        z: -524.263
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_flower_01.mesh"

        DefaultMaterial {
            id: mI_flower_01_Inst_material
            diffuseColor: "#ff55583f"
        }
        materials: [
            mI_flower_01_Inst_material
        ]
    }
    Model {
        id: sM_flower_02
        x: -25.1341
        y: 1.49012e-06
        z: -514.669
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_flower_02.mesh"
        materials: [
            mI_flower_01_Inst_material,
            mI_flower_01_Inst_material
        ]
    }
    Model {
        id: sM_grass_01
        x: -400.507
        y: 1.19209e-05
        z: -509.474
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_grass_01.mesh"

        DefaultMaterial {
            id: mI_grass_01_Inst_material
            diffuseColor: "#ff63684a"
        }
        materials: [
            mI_grass_01_Inst_material
        ]
    }
    Model {
        id: sM_grass_02
        x: -178.772
        y: 5.96046e-06
        z: -460.568
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_grass_02.mesh"
        materials: [
            mI_flower_01_Inst_material
        ]
    }
    Model {
        id: sM_grass_03
        x: 32.9274
        y: 3.72529e-07
        z: -403.154
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_grass_03.mesh"
        materials: [
            mI_flower_01_Inst_material
        ]
    }
    Model {
        id: sM_shrub_01
        x: 901.413
        y: 81.8369
        z: -149.441
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_shrub_01.mesh"

        DefaultMaterial {
            id: mI_foliage_01_Inst_material
            diffuseColor: "#ff373737"
        }
        materials: [
            mI_foliage_01_Inst_material
        ]
    }
    Model {
        id: sM_shrub_02
        x: 523.26
        z: -165.881
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_shrub_02.mesh"
        materials: [
            mI_flower_01_Inst_material
        ]
    }
    Model {
        id: sM_shrub_flower_01
        x: -891.735
        z: -169.954
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_shrub_flower_01.mesh"
        materials: [
            mI_flower_01_Inst_material
        ]
    }
    Model {
        id: sM_small_stone_02
        x: 669.99
        z: -196.019
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_small_stone_02.mesh"

        DefaultMaterial {
            id: mI_small_branch_Inst_001_material
            diffuseColor: "#ff373737"
        }
        materials: [
            mI_small_branch_Inst_001_material
        ]
    }
    Model {
        id: sM_small_stone_04
        x: -463.632
        y: 7.45058e-07
        z: -172.572
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_small_stone_04.mesh"
        materials: [
            mI_small_branch_Inst_001_material
        ]
    }
    Model {
        id: sM_stone_01
        x: -265.663
        y: 110.057
        z: 1429.39
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_stone_01.mesh"

        DefaultMaterial {
            id: mI_stone_01_Inst_material
            diffuseColor: "#ff373737"
        }
        materials: [
            mI_stone_01_Inst_material
        ]
    }
    Model {
        id: sM_stone_02
        x: -1361.33
        y: 115.727
        z: -73.7847
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_stone_02.mesh"
        materials: [
            mI_stone_01_Inst_material
        ]
    }
    Model {
        id: sM_stone_03
        x: -1705.43
        y: 124.166
        z: 727.692
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_stone_03.mesh"
        materials: [
            mI_stone_01_Inst_material
        ]
    }
    Model {
        id: sM_stone_04
        x: -602.44
        y: 110.057
        z: 490.258
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_stone_04.mesh"
        materials: [
            mI_stone_01_Inst_material
        ]
    }
    Model {
        id: sM_stone_05
        x: 157.334
        y: 155.208
        z: 1902.85
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_stone_05.mesh"
        materials: [
            mI_stone_01_Inst_material
        ]
    }
    Model {
        id: sM_stone_06
        x: 794.163
        y: 214.407
        z: 1237.73
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_stone_06.mesh"

        DefaultMaterial {
            id: mI_stone_cliff_Inst_material
            diffuseColor: "#ff373737"
        }
        materials: [
            mI_stone_cliff_Inst_material
        ]
    }
    Model {
        id: sM_branch_01
        z: -791.18
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_branch_01.mesh"

        DefaultMaterial {
            id: mI_branch_stump_Inst_material
            diffuseColor: "#ff373737"
        }
        materials: [
            mI_branch_stump_Inst_material
        ]
    }
    Model {
        id: sM_branch_02
        x: 691.635
        y: 36.6855
        z: -570.85
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_branch_02.mesh"
        materials: [
            mI_branch_stump_Inst_material
        ]
    }
    Model {
        id: sM_log_01
        x: -679.062
        y: 36.6855
        z: -661.138
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_log_01.mesh"

        DefaultMaterial {
            id: mI_log_stump_Inst_material
            diffuseColor: "#ff373737"
        }
        materials: [
            mI_log_stump_Inst_material
        ]
    }
    Model {
        id: sM_log_02
        x: -1801.96
        y: -2.38419e-05
        z: -499.653
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_log_02.mesh"

        DefaultMaterial {
            id: mI_log_02_Inst_material
            diffuseColor: "#ff373737"
        }
        materials: [
            mI_log_02_Inst_material
        ]
    }
    Model {
        id: sM_small_branch_01
        x: -1009.49
        y: -1.19209e-05
        z: 186.332
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_branch_01.mesh"
        materials: [
            mI_small_branch_Inst_001_material
        ]
    }
    Model {
        id: sM_small_branch_02
        x: -458.949
        y: -5.96046e-06
        z: -351.103
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_branch_02.mesh"
        materials: [
            mI_small_branch_Inst_001_material
        ]
    }
    Model {
        id: sM_small_branch_03
        x: -679.96
        y: 2.98023e-06
        z: -107.942
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_small_branch_03.mesh"
        materials: [
            mI_small_branch_Inst_001_material
        ]
    }
    Model {
        id: sM_stump_01
        x: -296.022
        y: 36.6855
        z: -8.05064
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_stump_01.mesh"
        materials: [
            mI_log_stump_Inst_material
        ]
    }
    Model {
        id: sM_stump_02
        x: -800.543
        y: 81.8369
        z: -285.743
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_stump_02.mesh"
        materials: [
            mI_branch_stump_Inst_material
        ]
    }
    Model {
        id: sM_stump_03
        x: 267.813
        y: 110.057
        z: 619.871
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_stump_03.mesh"

        DefaultMaterial {
            id: mI_stump_03_Inst_material
            diffuseColor: "#ff373737"
        }
        materials: [
            mI_stump_03_Inst_material
        ]
    }
    Model {
        id: sM_tree_01
        x: 1570.57
        z: 648.103
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_tree_01.mesh"

        DefaultMaterial {
            id: mI_tree_bark_01_Inst_material
            diffuseColor: "#ff373737"
        }
        materials: [
            mI_foliage_01_Inst_material,
            mI_tree_bark_01_Inst_material
        ]
    }
    Model {
        id: sM_tree_02
        x: -1000.77
        y: -2.38419e-05
        z: 1526.65
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_tree_02.mesh"
        materials: [
            mI_foliage_01_Inst_material,
            mI_tree_bark_01_Inst_material
        ]
    }
    Model {
        id: sM_tree_03
        x: -876.411
        z: -1164.92
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_tree_03.mesh"
        materials: [
            mI_foliage_01_Inst_material,
            mI_tree_bark_01_Inst_material
        ]
    }
    Model {
        id: sM_tree_04
        x: 1217.01
        z: 1047.34
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_tree_04.mesh"
        materials: [
            mI_foliage_01_Inst_material,
            mI_tree_bark_01_Inst_material
        ]
    }
    Model {
        id: sM_tree_05
        x: 1432.89
        z: -66.7551
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_tree_05.mesh"

        DefaultMaterial {
            id: mI_tree_bark_01_Inst_004_material
            diffuseColor: "#ff373737"
        }
        materials: [
            mI_tree_bark_01_Inst_004_material,
            mI_stone_01_Inst_material
        ]
    }
    Model {
        id: sM_tree_06
        x: 1030.94
        y: -5.96046e-06
        z: -882.974
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_tree_06.mesh"
        materials: [
            mI_tree_bark_01_Inst_004_material,
            mI_stone_01_Inst_material
        ]
    }
    Model {
        id: sM_tree_shrub
        x: -912.698
        y: 36.6855
        z: 516.325
        rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
        source: "qrc:/model/meshes2/sM_tree_shrub.mesh"
        materials: [
            mI_tree_bark_01_Inst_004_material,
            mI_stone_01_Inst_material
        ]
    }
}
